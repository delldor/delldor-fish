function actunoporuno --description 'Actualiza uno por uno los paquetes'
	echo "Comenzando a actualizar uno por uno por tamaño"
	for i in (aptitude search ~U '-F %p'|xargs apt-cache --no-all-versions show| awk '$1 == "Package:" { p = $2 }; $1 == "Size:"    { printf("%d %s\n", $2, p) }'|sort -n|cut -d' ' -f2)
		echo "Se va a actualizar $i. Esperando $tiempo segundos para continuar"
		sleep $tiempo
		sudo -s apt-get install --no-remove --assume-no --allow-unauthenticated $i
	end
end
