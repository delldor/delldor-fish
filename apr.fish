# Defined in - @ line 0
function apr --description 'alias apr=sudo aptitude-robot'
	set temporal (mktemp)
	for i in (aptitude search ~U -F '%p\$%V'| grep -e +b -e beta -e ~rc -e ~pre -e ~alpha|cut -d\$ -f1);
      echo "= $i" | tee -a $temporal
    end
    sudo cp -v $temporal /etc/aptitude-robot/pkglist.d/zzz_betas_automatico
    sudo aptitude-robot
end
